--[[
	Ball class:
	controls the ball
]]

require("../AABB")

Ball = {
	x, y,
	startX, startY,
	speedX, speedY,
	width = 8, height = 8,
	speed = 256,
	p1Score = 0,
	p2Score = 0
}

function Ball:new(x, y)
	o = {}

	setmetatable(o, self)
	self.__index = self
	o.startX = x
	o.startY = y
	o.x = x
	o.y = y
	o.speedY = math.random(-128, 128)
	o.speedX = math.random(-4, 4) * 64

	return o
end

function Ball:update(delta)

	-- Make the ball gain speed constantly
	if(self.speedX < 0) then
		self.speedX = self.speedX - 4 * delta
	else
		self.speedX = self.speedX + 4 * delta
	end

	self.x = self.x + self.speedX * delta
	self.y = self.y + self.speedY * delta

	-- handle scoring
	if(self.x < 0) then
		self.p1Score = self.p1Score + 1
		self:reset()
	end
	if(self.x > 480) then
		self.p2Score = self.p2Score + 1
		self:reset()
	end

	-- reset ball if r is pressed
	if love.keyboard.isDown("r") then
		self:reset()
	end

	self:checkBounds()
end

-- check if still inside the screen
function Ball:checkBounds()
	if(self.y + (self.height / 2) > 360) then
		self.speedY = -self.speedY
		self.y = 360 - (self.height / 2)
	end
	if(self.y - (self.height / 2) < 0) then
		self.speedY = -self.speedY
		self.y = 0 + (self.height / 2)
	end
end

-- Check if ball hit a paddle (this should be called in the update loop after the update)
function Ball:checkCollision(delta, paddle)
	if(checkCollision(self.x, self.y, self.width, self.height, paddle.x, paddle.y, paddle.width, paddle.height)) then
		self.speedX = -self.speedX

		local paddleMiddle = paddle.y
		local ballMiddle   = self.y

		self.speedY = -(paddleMiddle - ballMiddle) * 8
		
		-- the ball gets stuck inside the paddle sometimes so we need to make it move outside of the paddle
		if(self.x < paddle.x) then
			self.x = self.x - 128 * delta
		else
			self.x = self.x + 128 * delta
		end
	end
end

-- Return ball to original position
function Ball:reset()
	self.x = self.startX
	self.y = self.startY
	self.speedY = math.random(-128, 128)
	self.speedX = math.random(-128, 128)
end

function Ball:render()
	-- render the ball
	love.graphics.setColor(255, 0, 255, 255)
	love.graphics.rectangle("line", self.x - (self.width / 2), self.y - (self.height / 2), self.width, self.height)
	
	-- render the score
	love.graphics.print(self.p1Score, (480 / 2) - 64, 16)
	love.graphics.print(self.p2Score, (480 / 2) + 64, 16)
end
