--[[
	Paddle class:
	controls the paddle the player and AI use
]]

Paddle = {
	x, y,
	isPlayer,
	width = 16, height = 64,
	speed = 180
}

-- Paddle constructor
function Paddle:new(x, y, isPlayer)
	o = {}

	setmetatable(o, self)
	self.__index = self
	o.x = x
	o.y = y
	o.isPlayer = isPlayer

	return o
end

-- Update function should be called every 'step'
function Paddle:update(delta, ball)
	if(self.isPlayer) then
		self:playerUpdate(delta)
	else
		self:aiUpdate(delta, ball)
	end
	self:checkBounds()
end

function Paddle:playerUpdate(delta)
	-- check if up/down is pressed and add it to the speed
	local movement = 0
	if love.keyboard.isDown("up") then
		movement = movement - (self.speed * delta)
	end
	if love.keyboard.isDown("down") then
		movement = movement + (self.speed * delta)
	end

	-- add the movement to our Y position
	self.y = self.y + movement
end

-- AI update gets called if the paddle is AI controlled
function Paddle:aiUpdate(delta, ball)
	-- follow the ball
	local movement = 0
	if(self.y > ball.y) then
		movement = movement - (self.speed * delta)
	end
	if(self.y < ball.y) then
		movement = movement + (self.speed * delta)
	end

	self.y = self.y + movement
end

-- check if inside the screen
function Paddle:checkBounds()
	if(self.y + (self.height / 2) > 360) then
		self.y = 360 - (self.height / 2)
	end
	if(self.y - (self.height / 2) < 0) then
		self.y = 0 + (self.height / 2)
	end
end

-- Render the Paddle
function Paddle:render()
	love.graphics.setColor(255, 0, 255, 255)
	love.graphics.rectangle("line", self.x - (self.width / 2), self.y - (self.height / 2), self.width, self.height)
end
