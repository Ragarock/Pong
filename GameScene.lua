require("Scene")

GameScene = {
}

function GameScene:new()
	o = {}

	setmetatable(o, self)
	self.__index = self

	return o
end

function GameScene:keyPressed(key, main)
end

function GameScene:init()
	player = Paddle:new(32, 360/2, true)
	enemy  = Paddle:new(480 - 32, 360/2, false)
	ball = Ball:new(480/2, 360/2)
end

function GameScene:stop()

end

function GameScene:update(delta)
	ball:update(delta)
	player:update(delta)
	enemy:update(delta, ball)

	-- check collisions
	ball:checkCollision(delta, player)
	ball:checkCollision(delta, enemy)
end

function GameScene:render()
	ball:render()
	player:render()
	enemy:render()
end
