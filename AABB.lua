-- Returns true if 2 boxes are colliding

function checkCollision(x1, y1, w1, h1, x2, y2, w2, h2)
	return x1-(w1/2) < x2+(w2/2) and
	       x2-(w2/2) < x1+(w1/2) and
         y1-(h1/2) < y2+(h2/2) and
				 y2-(h2/2) < y1+(h1/2)
end
