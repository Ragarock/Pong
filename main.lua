require("entities/Paddle")
require("entities/Ball")
require("Scene")
require("GameScene")
require("MenuScene")

-- current scene that will get update and draw calls
currentScene = nil

function love.load()
	-- set the randomness seed to the current time
	math.randomseed(os.time())
	
	-- WINDOW SETUP
	love.window.setMode(480, 360, {fullscreen = false, vsync = false, resizable = false})

	-- FONT SETUP
	font = love.graphics.newFont("fonts/gohufont-11.ttf", 11)
	love.graphics.setFont(font)

	-- Clear colour
	love.graphics.setBackgroundColor(0, 0, 0)

	-- SCENE SETUP
	scenes = {
		MenuScene:new(),
		GameScene:new(),
	}
	setCurrentScene("menu")
end

-- makes it possible to check what key was pressed in the scene
function love.keypressed(key)
	currentScene:keyPressed(key, setCurrentScene)
end

-- passes the update to the scene
function love.update(delta)
	currentScene:update(delta)
end

-- passes the draw to the scene
function love.draw()
	currentScene:render()
	love.graphics.print(tostring(love.timer.getFPS()), 10, 10)
end

-- sets the current scene
function setCurrentScene(name)
	scene = nil

	-- check if the scene exists
	if     name == "menu" then scene = scenes[1]
	elseif name == "game" then scene = scenes[2]
	else   print("scene does not exist!"); os.exit(0)
	end

	-- stop the currentscene
	if(not currentScene == nil) then
		currentScene.stop()
	end
	-- initialize the new current scene
	currentScene = scene
	currentScene:init()
end
