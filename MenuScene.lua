-- The menu
MenuScene = {
	options = {
		"Start",
		"Credits",
		"Quit",
	},
	currentOption = 1,
}

function MenuScene:new()
	o = {}

	setmetatable(o, self)
	self.__index = self

	return o
end

function MenuScene:keyPressed(key, main)
	-- menu options handling
	if key == "up" then
		self.currentOption = self.currentOption - 1
		if(self.currentOption < 1) then
			self.currentOption = table.getn(self.options)
		end
	end
	if key == "down" then
		self.currentOption = self.currentOption + 1
		if(self.currentOption > table.getn(self.options)) then
			self.currentOption = 1
		end
	end
	if key == "return" then
		local option = self.options[self.currentOption]
		if     option == "Start"   then main("game")
		elseif option == "Credits" then print("Ragarock made dis")
		elseif option == "Quit"    then os.exit()
		end
	end
end

function MenuScene:init()
end

function MenuScene:stop()
end

function MenuScene:update(delta)
end

function MenuScene:render()
	-- render the menu options
	-- set the colour to pink if its the currently selected option
	-- else keep it white
	for i = 1, table.getn(self.options) do
		if(i == self.currentOption) then
			love.graphics.setColor(255, 0, 255, 255)
		else
			love.graphics.setColor(255, 255, 255, 255)
		end
		love.graphics.print(self.options[i], 16, 90 * i)
	end
end
