Scene = {
}

function Scene:new()
	o = {}

	setmetatable(o, self)
	self.__index = self

	return o
end

function Scene:init()
	print("scene")
end

function Scene:keyPressed(key, main)
end

function Scene:stop()
end

function Scene:update(delta)
end

function Scene:render()
end
